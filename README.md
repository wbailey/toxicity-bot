# Text Toxicity Bot

Proof of concept NLP text toxicity classifier for potential use as a Reddit automod bot using tensorflow.js.

## Links and Sources

### Links

* [reddit bots link](https://www.reddit.com/prefs/apps)
* [reddit oAuth quickstart](https://github.com/reddit-archive/reddit/wiki/OAuth2-Quick-Start-Example)
* [reddit api docs](https://www.reddit.com/dev/api/)

### Sources

* [demo code](https://github.com/tensorflow/tfjs-models/tree/master/toxicity)
* [overview](https://medium.com/tensorflow/text-classification-using-tensorflow-js-an-example-of-detecting-offensive-language-in-browser-e2b94e3565ce)
* [universal sentance encoder](https://arxiv.org/pdf/1803.11175.pdf)
* [training repo](https://github.com/conversationai/conversationai-models/tree/main/experiments)
* [TF content moderation](https://blog.tensorflow.org/2022/08/content-moderation-using-machine-learning-a-dual-approach.html)

