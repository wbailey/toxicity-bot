const { 
	delay, 
} = require("./src/api");
const {
	initBot,
	moderateToxicComments,
	respondToMessages
} = require("./src/bot");

const main = async () => {
	await initBot();
	while (true) {
		try {
			await moderateToxicComments();
			await respondToMessages();
			await delay(1000 * 60);
		} catch (err) {
			console.log("error in main loop", err);
			await delay(1000 * 60);
		}
	}
}

main();
