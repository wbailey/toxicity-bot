require("dotenv").config()
const snoowrap = require("snoowrap");

const {
	CLIENT_ID,
	CLIENT_SECRET,
	USER_NAME,
	PASSWORD,
	USER_AGENT,
	SUB
} = process.env

let requester = null;
let sub = null;

const loadRequester = async () => {
	// create new api consumer
	requester = new snoowrap({
		userAgent: USER_AGENT,
		clientId: CLIENT_ID,
		clientSecret: CLIENT_SECRET,
		username: USER_NAME,
		password: PASSWORD
	})
	// find the sub
	sub = await requester.getSubreddit(SUB);
}


// lazy way to regulate api call speed for now
const delay = async milliseconds => {
	return new Promise(resolve => {
		setTimeout(resolve, milliseconds);
	});
}

const getComments = async () => await sub.getNewComments();
const getMessages = async () => await requester.getInbox({ filter: "unread" });
const newModMail = async (body, subject = `message from ${USER_NAME}`, srName = SUB) => requester.createModmailDiscussion({ body, subject, srName});
const getMyModMail = async () => await requester.getModmail();

module.exports = {
	loadRequester,
	delay,
	getComments,
	getMessages,
	newModMail,
	getMyModMail
}

