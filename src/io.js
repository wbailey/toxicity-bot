const {
    existsSync,
    readFileSync,
    writeFileSync
} = require('fs');

const toxicCommentsFile = "./data/toxicComments.json";

const withToxicCommentsJson = callback => {
	if (!existsSync(toxicCommentsFile)) {
		writeFileSync(toxicCommentsFile, JSON.stringify({
			deleted: [],
			lastReadTime: null,
		}));
	}

	const toxicCommentsJson = JSON.parse(readFileSync(toxicCommentsFile));
	callback(toxicCommentsJson);
	writeFileSync(toxicCommentsFile, JSON.stringify(toxicCommentsJson));
}

module.exports = {
	withToxicCommentsJson
}
