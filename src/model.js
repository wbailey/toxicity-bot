const toxicity = require("@tensorflow-models/toxicity");
const chalk = require("chalk");
require("dotenv").config();

const { THRESHOLD } = process.env

let model = null;

const loadModel = async () => {
	try {
		model = await toxicity.load(Number(THRESHOLD));
		return model;
	} catch (err) {
		console.log("error loading model", err);
	}
}

const predict = async comment => await model.classify([comment])

const formatPrediction = prediction => {
	const formattedPrediction = { isToxic: false };
	prediction.forEach(classifacation => {
		const [results] = classifacation.results
		formattedPrediction[classifacation.label] = {
			likelyNotToxic: results.probabilities[0],
			likelyToxic: results.probabilities[1],
			match: results.match
		}
		if (results.match === true || results.match === null) {
			formattedPrediction.isToxic = true;
		}
	});
	return formattedPrediction;
}

const printPrediction = (prediction, comment = null) => {
	const threshold = Number(THRESHOLD);
	let str = `prediction ${comment ? "for '" + comment + "'" : ""}`;
	let likelyToxic = false;
	Object.values(prediction).forEach(attribute => {
		const { match } = attribute.results[0];
		const [probOne, probTwo] = attribute.results[0].probabilities;
		const color = match ? chalk.bgRed : match === null ? chalk.bgYellow : chalk.bgGreen;
		if (match || match === null) {
			likelyToxic = true;
		}

		str += "\n\t"
		str += `${attribute.label}: ${chalk.black(color(match))}, likely non-toxic: ${probOne <= threshold ? chalk.red(probOne) : probOne}, likely toxic: ${probTwo >= threshold ? chalk.red(probTwo) : probTwo}`;
	});
	console.log(str);
	console.log(`Verdict: ${likelyToxic ? chalk.red("comment is toxic.") : "comment is non-toxic"}`, "\n"); 
}



module.exports = {
	loadModel,
	predict,
	formatPrediction,
	printPrediction
}
