require("dotenv").config()
const { 
	loadRequester,
	getComments,
	getMessages
} = require("./api");
const {
	loadModel,
	predict,
	formatPrediction
} = require("./model");
const { withToxicCommentsJson } = require("./io");
const { 
	THRESHOLD,
	PRIVILEDGED_USERS
} = process.env

let priviledgedUsers = null;

const initBot = async () => {
	// setup model and api
	await loadModel();
	await loadRequester();
	priviledgedUsers = PRIVILEDGED_USERS.split(",");
}

const moderateToxicComments = async () => {
	// get the new comments
	const newComments = await getComments();
	// skip deleted and already banned comments
	const newCommentsFilteredByDeleted = newComments.filter(comment => comment.link_author !== "[deleted]")
		.filter(comment => comment.banned_by === null);
		
	// predict all the comments and recombine them into a simplified object for ease of manipulation	
	const pendingPredictions = newCommentsFilteredByDeleted.map(comment => predict(comment.body));
	const predictions = await Promise.all(pendingPredictions);
	const formattedPredictions = predictions.map(prediction => formatPrediction(prediction));
	const commentsZippedWithPredictions = newCommentsFilteredByDeleted.map((comment, i) => {
		return {
			instance: comment,
			prediction: formattedPredictions[i]
		}
	});

	// identify toxic comments to reply and remove them
	const toxicComments = commentsZippedWithPredictions.filter(comment => comment.prediction.isToxic);
	const toxicCommentsWithResponses = toxicComments.map(comment => {
		let message = "Hi there 👋. I am the text toxicty bot. I am removing your comment because I have detected that it may be toxic in the following areas:\n\n"
		const { prediction } = comment
		const threshold = Number(THRESHOLD);
		for (const label in prediction) {
			if (label === "isToxic") {
				continue;
			}
			if (prediction[label].match === true || prediction[label].match === null) {
				message += "**" + label.replace("_", " ") + "**\n";
				if (prediction[label].likelyNotToxic <= threshold) {
					message += `\n_${(prediction[label].likelyNotToxic * 100).toFixed(2)}% chance this comment is non-toxic in this area which lower than the acceptable minimum threshold of ${(threshold * 100).toFixed(2)}%._`;
				} 
				if (prediction[label].likelyToxic >= threshold) {
					message += `\n_${(prediction[label].likelyToxic * 100).toFixed(2)}% chance this comment is toxic in this area which is greater than the acceptable minimum threshold of ${(threshold * 100).toFixed(2)}%._`;
				}
				message += "\n\n";
			}
		}
		message += "Beep beep, boop boop. This action was performed automatically by a bot 🤖.\n\n" 
		message += "If you believe it was an error please reach out to the mod team to restore your comment.\n\n"
		message += "Read my source code [here](https://gitlab.com/wbailey/toxicity-bot) licensed [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)."
		return { ...comment, message }
	});
	
	// const pendingCommentReplies = toxicCommentsWithResponses.map(comment =>	comment.instance.reply(comment.message));
	// await Promise.all(pendingCommentReplies);

	// const pendingCommentRemovals = toxicCommentsWithResponses.map(comment => comment.instance.remove());
	// await Promise.all(pendingCommentRemovals);

	// write new toxic comments to file
	withToxicCommentsJson(toxicCommentsJson => {
		toxicCommentsWithResponses.forEach(toxicComment => {
			toxicCommentsJson.deleted.push({
				link_url: toxicComment.instance.link_url,
				link_author: toxicComment.instance.link_author,
				body: toxicComment.instance.body,
				link_id: toxicComment.instance.link_id,
				prediction: toxicComment.prediction,
				readBy: []
				
			});
		});
	});
	
	console.log(`deleting ${toxicCommentsWithResponses.length} toxic comments`);
	return toxicCommentsWithResponses;

}


const respondToMessages = async () => {
	const commands = [ "unreads", "unread", "reads", "read", "all", "help"];
	const messages = await getMessages();
	// only respond to authorized users
	const messagesFromPriviledgedUsers = messages.filter(message => priviledgedUsers.includes(message.author.name));
	// parsing commands and building a reponse string	
	const messagesWithResponses = messagesFromPriviledgedUsers.map(message => {
		const splitBody = message.body.split(" ");
		const commandsToExecute = splitBody.filter(word => commands.includes(word));
		if (commandsToExecute.length === 0) {
			commandsToExecute.push("help");
		}
		let responseText = `Hi u/${message.author.name} 👋! I hope you are having a wonderful day.\n\n`
		withToxicCommentsJson(toxicCommentsJson => { 
			commandsToExecute.forEach(command => {
				let comments = [];
				switch(command) {
					case "unread": 
					case "unreads": {
						comments = toxicCommentsJson.deleted.filter(comment => {
							if (!comment.readBy.includes(message.author.name)) {
								comment.readBy.push(message.author.name)

								return true;
							}
							return false;
						});
						responseText += `**unreads** (${comments.length}):\n\n`;
						break;
					}
					case "read":
					case "reads": {
						comments = toxicCommentsJson.deleted.filter(comment => comment.readBy.includes(message.author.name));
						responseText += `**reads** (${comments.length}):\n\n`;
						break;
					}
					case "all": {
						comments = toxicCommentsJson.deleted;
						toxicCommentsJson.deleted.forEach(comment => {
							if (!comment.readBy.includes(message.author.name)) {
								comment.readBy.push(message.author.name)
							}
						});
						
						responseText += `**all** (${comments.length}):\n\n`;
						break;
					}
					case "help": {
						responseText += `**help**:\n\n`
						responseText += "I respond to the following commands (place them in your messges to me and I will pick up on them as keywords and respond accordingly):\n\n";
						responseText += commands.reduce((prev, curr) => prev += ` **${curr}**\n\n`, "");
						responseText += "."
						break;
					}
					default: {
						responseText += `I have had an error and I parsed ${command} as a command.`;
					}
				}
				comments.forEach(comment => {
					responseText += `u/${comment.link_author} commented "${comment.body}" [link here](${comment.link_url}). I detected toxicity in the following areas: `;
					for (const label in comment.prediction) {
						if (label === "isToxic") {
							continue;
						}
						if (comment.prediction[label].match === true || comment.prediction[label].match === null) {
							responseText += ` ${label.replace("_", " ")}`;
						}
					}
					responseText += ".\n\n";
				});
			});
		});

		responseText += "\n\n";
		responseText += "Is there anything help I can help you with?";
		return { instance: message, responseText }
	});
	// reply all
	const pendingReplies = messagesWithResponses.map(message => message.instance.reply(message.responseText));
	await Promise.all(pendingReplies);
	// mark them as read so they don't get replied again
	const pendingMessagesMarkedRead = messagesWithResponses.map(message => message.instance.markAsRead());
	await Promise.all(pendingMessagesMarkedRead);
	// if there are any randos in the inbox, just delete
	const messagesFromNonPriviledgedUsers = messages.filter(message => !priviledgedUsers.includes(message.author.name));
	const pendingMessageDeletion = messagesFromNonPriviledgedUsers.map(message => message.instance.deleteFromInbox());
	await Promise.all(pendingMessageDeletion);
}

module.exports = {
	initBot,
	moderateToxicComments,
	respondToMessages
}

